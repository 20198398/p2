#Realizar un programa que reciba por teclado el sueldo de un empleado y le  
# aplique los cálculos de ISR (ver tabla DGII), ARS, y AFP (investigar porcentajes).
def isr():
    anual = salario * 12
    if anual >= 416220.01 and anual <= 624329.00 :
        calculo= round(((anual - 624329.00)* 0.15)/ 12)
        print("-Descuento de ISR: $", calculo)
    elif anual >= 624329.01 and anual <= 867123.00 :
        calculo= round((((anual -624329.00)* 0.20) + 31216.35)/ 12)
        print("-Descuento de ISR: $", calculo)
    elif anual >= 867123.01:
         calculo= round((((anual - 867123.01)* 0.25) + 79776.00)/ 12)
         print("-Descuento de ISR: $", calculo)
    else:
        print("-No se aplica descuento ISR (Exento)")

def afp():
    calc = round(salario * 0.07) 
    print("-Descuento de AFP: $", calc)

def ars():
    calc = round(salario * 0.03) 
    print("-Descuento de ARS: $", calc)

while True:

    print("--Calcular descuentos ISR, ARS y AFP--")
    salario = float(input("Ingrese salario: "))
    print()
    isr()
    print()
    ars()
    print()
    afp()
    break  



