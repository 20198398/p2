multiplos_5=[]
 
for i in range(1,101): #Para sacar los múltiplos de 5
 
        multiplos_5.append(i)
 
print("Los multiplos de 5 son:", multiplos_5)

tabla_desde = 1
tabla_hasta = 100 
desde = 1 
hasta = 12
print("Tablas de multiplicar de los múltiplos de 5:")
#Para sacar las tablas de multiplicar de los múltiplos del 5, comprendiendo desde el 1 hasta el 100
for f1 in range(tabla_desde, tabla_hasta + 1):
	print(f'Tabla de multiplicar del {f1}:') 
	for f2 in range(desde, hasta + 1):
		print(f'{f1} x {f2} = {f1 * f2}')
	print() 